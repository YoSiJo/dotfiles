#!/usr/bin/env bash

_string="Hello world."

function __echo() {
  echo -n "${1}"
}

__echo "${_string}"
